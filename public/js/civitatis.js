
$( document ).ready(function() {
    $("#search").submit(function(e) {

        e.preventDefault(); 
    
        var form = $(this);
        var url = form.attr('action');
        console.log(form.serialize());
        $.ajax({
               type: "POST",
               url: url,
               headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
               data: form.serialize(),
               success: function(data)
               {
                $(".ajax-response").html(data);
                 
               }
             });
    
    
    });

    $('body').on('click', '#comprar', function (){
      
      var base_url = window.location.origin+window.location.pathname;
      
      var id = $(this).data("id");
      var personNumber = $("#personNumber").val();
      var date = $("#date").val();
      
      $.ajax({
             type: "POST",
             url: base_url+"/api/save-activity",
             headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
             data: {"id":id, "personNumber":personNumber,"date":date}, 
             success: function(data)
             {
              $(".ajax-response").html('hemos reservado su actividad');
             }
           });
    });


});
