<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Home</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    </head>
    <body>
<form id="search" action="{{ action('Api\HomeController@getActivity') }}">
        <div class="form-group">
            <label for="exampleInputEmail1">Fecha</label>
            <input type="date" name="date" class="form-control" id="date">
        </div>

        <div class="form-group">
            <label for="personNumber">Numero de peronas</label>
            <input type="text" name="personNumber" class="form-control" id="personNumber">
        </div>
        
        <button type="submit" class="btn btn-primary">Buscar</button>
    </form>

    <div class="ajax-response">

    </div>

        <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/civitatis.js') }}"></script>
    </body>
</html>
