<table class="table">
  <thead>
    <tr>
      <th scope="col">titulo</th>
      <th scope="col">precio total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($activityTotal as $activity)
        <tr>
                <td>{{ $activity->title }}</td>
                <td>{{ $activity->price*$personNumber }}</td>
                <td><button type="button" class="btn btn-primary" id="comprar" data-id="{{ $activity->id }}">Comprar</button></td>
                
        </tr>
    @endforeach
  </tbody>
</table>