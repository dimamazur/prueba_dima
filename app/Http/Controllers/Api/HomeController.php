<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Activity;
use App\Reservation;
use Carbon\Carbon;
class HomeController extends Controller
{

    private $html;

    public function getActivity(){

        $dataForm = Input::all();

        $dateActivity = $dataForm['date'];
        $personNumber = $dataForm['personNumber'];

        $activityTotal = Activity::where('date_fin','>', $dateActivity)->where('date_ini','<', $dateActivity)->orderBy('popularity', 'DESC')->get(); 
        
        $this->html = view('partials.activity', compact('activityTotal','personNumber'))->render();

        return $this->html;
        
    }

    public function saveActivity(){

        $id = Input::get('id');

        $personNumber = Input::get('personNumber');

        $date = Input::get('date');

        $activity = Activity::find($id);

        $reservation = new Reservation;

        $reservation->activity_id = $id;

        $reservation->person_number = $personNumber;

        $reservation->price = $activity->price*$personNumber;
        
        $reservation->created_at = Carbon::now();
        $reservation->updated_at = Carbon::now();

        $reservation->date_reservation_activity = $date;

        $reservation->save();
        
    }

}
