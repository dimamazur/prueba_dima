<?php

namespace App\Listeners;

use App\Events\AddDate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddDateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddDate  $event
     * @return void
     */
    public function handle(AddDate $event)
    {
        //dump($event->article);
    }
}
